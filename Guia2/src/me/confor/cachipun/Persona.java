package me.confor.cachipun;

// la guia pide un objeto, no una clase :p
enum Mano {
	PIEDRA,
	PAPEL,
	TIJERA,
	NADA
}

public class Persona {
	private Mano izq = Mano.NADA;
	private Mano der = Mano.NADA;

	// "setter" de manos
	public void jugar(Mano jugada) {
		this.izq = jugada;
	}

	public void jugar(Mano jugadaIzquierda, Mano jugadaDerecha) {
		this.izq = jugadaIzquierda;
		this.der = jugadaDerecha;
	}

	public Mano getMano() {
		return this.izq;
	}

	public Mano getManoIzq() {
		return this.izq;
	}

	public Mano getManoDer() {
		return this.der;
	}
}
