package me.confor.cachipun;

import java.util.Scanner;

public class Main {
	static void println(String string) {
		System.out.println(string);
	}

	// no cerrar stdin!
	@SuppressWarnings("resource")
	static String prompt(String prompt) {
		System.out.print(prompt);
		return new Scanner(System.in).nextLine();
	}

	static Mano pedirJugada(String prompt) {
		String input = prompt(prompt);

		if (input.equalsIgnoreCase("piedra"))
			return Mano.PIEDRA;
		else if (input.equalsIgnoreCase("papel"))
			return Mano.PAPEL;
		else if (input.equalsIgnoreCase("tijera"))
			return Mano.TIJERA;
		else
			return Mano.NADA;
	}

	static void juegoSimple() {
		Juego juego = new Juego();
		Mano inputUno = pedirJugada("Jugador UNO, ¿piedra, papel o tijera? ");
		Mano inputDos = pedirJugada("Jugador DOS, ¿piedra, papel o tijera? ");

		if (inputUno == Mano.NADA) {
			println("Jugador UNO: jugada inválida");
		}

		if (inputDos == Mano.NADA) {
			println("Jugador DOS: jugada inválida");
		}

		juego.getJugadorUno().jugar(inputUno);
		juego.getJugadorDos().jugar(inputDos);

		Estado estado = juego.ganadorSimple();

		if (estado == Estado.EMPATE) {
			println("¡Empate! El juego comenzará de nuevo.");
			juegoSimple();
		} else if (estado == Estado.GANA_UNO) {
			println("Gana jugador UNO");

			// de la guía: "El ganador deberá levantar sus manos en señal de
			// victoria y el perdedor bajarlas."
			println("\\O/     O");
			println(" |     /|\\");
			println("/ \\    / \\");
			println("");
			println(" 1      2 ");

		} else if (estado == Estado.GANA_DOS) {
			println("Gana jugador DOS");

			println(" O     \\O/");
			println("/|\\     |");
			println("/ \\    / \\");
			println("");
			println(" 1      2 ");
		} else {
			println("¡Juego inválido!");
		}
	}

	static void juegoDoble() {
		Juego juego = new Juego();
		Mano inputUnoIzq = pedirJugada("Jugador UNO IZQUIERDA, ¿piedra, papel o tijera? ");
		Mano inputDosIzq = pedirJugada("Jugador DOS IZQUIERDA, ¿piedra, papel o tijera? ");
		Mano inputUnoDer = pedirJugada("Jugador UNO DERECHA, ¿piedra, papel o tijera? ");
		Mano inputDosDer = pedirJugada("Jugador DOS DERECHA, ¿piedra, papel o tijera? ");

		if (inputUnoIzq == Mano.NADA) {
			println("Jugador UNO IZQUIERDA: jugada inválida");
		}

		if (inputUnoDer == Mano.NADA) {
			println("Jugador UNO DERECHA: jugada inválida");
		}

		if (inputDosIzq == Mano.NADA) {
			println("Jugador DOS IZQUIERDA: jugada inválida");
		}

		if (inputDosDer == Mano.NADA) {
			println("Jugador DOS DERECHA: jugada inválida");
		}

		juego.getJugadorUno().jugar(inputUnoIzq, inputUnoDer);
		juego.getJugadorDos().jugar(inputDosIzq, inputDosDer);

		Estado[] estados = juego.ganadorDoble();
		Estado estadoIzquierdas = estados[0];
		Estado estadoDerechas = estados[1];

		if (estadoIzquierdas == Estado.EMPATE) {
			println("Empate en izquierdas!");
		} else if (estadoIzquierdas == Estado.GANA_UNO) {
			println("Gana jugador UNO en izquierdas!");
		} else if (estadoIzquierdas == Estado.GANA_DOS) {
			println("Gana jugador DOS en izquierdas!");
		} else {
			println("¡Alguien hizo algo inválido en las manos izquierdas!");
		}

		if (estadoDerechas == Estado.EMPATE) {
			println("Empate en derechas!");
		} else if (estadoDerechas == Estado.GANA_UNO) {
			println("Gana jugador UNO en derechas!");
		} else if (estadoDerechas == Estado.GANA_DOS) {
			println("Gana jugador DOS en derechas!");
		} else {
			println("¡Alguien hizo algo inválido en las manos derechas!");
		}

		if (estadoIzquierdas == Estado.EMPATE && estadoDerechas == Estado.EMPATE) {
			println("Ambas manos empataron, el juego comenzará de nuevo!");
			juegoDoble();
		}
	}

	public static void main(String[] args) {
		println("Piedra, papel o tijera");
		println("Escriba la opción y presione 'enter'");

		String modo = prompt("¿Qué modo quiere jugar? (simple|doble) ");

		if (modo.equalsIgnoreCase("doble")) {
			juegoDoble();
		} else {
			juegoSimple();
		}

		return;
	}
}
