package me.confor.cachipun;

enum Estado {
	EMPATE,
	GANA_UNO,
	GANA_DOS,
	INVALIDO
}

public class Juego {
	private Persona uno;
	private Persona dos;

	public Juego() {
		this.uno = new Persona();
		this.dos = new Persona();
	}

	public Persona getJugadorUno() {
		return this.uno;
	}

	public Persona getJugadorDos() {
		return this.dos;
	}

	public Estado ganadorSimple() {
		Mano manoUno = this.uno.getMano();
		Mano manoDos = this.dos.getMano();

		if (manoUno == Mano.NADA || manoDos == Mano.NADA)
			return Estado.INVALIDO;

		if (manoUno == manoDos)
			return Estado.EMPATE;

		// revisar todas las posibles maneras en las que ganaría un jugador
		Boolean[] ganaJugadorUno = {
			manoUno == Mano.PAPEL && manoDos == Mano.PIEDRA,
			manoUno == Mano.PIEDRA && manoDos == Mano.TIJERA,
			manoUno == Mano.TIJERA && manoDos == Mano.PAPEL
		};

		Boolean[] ganaJugadorDos = {
			manoDos == Mano.PAPEL && manoUno == Mano.PIEDRA,
			manoDos == Mano.PIEDRA && manoUno == Mano.TIJERA,
			manoDos == Mano.TIJERA && manoUno == Mano.PAPEL
		};

		// ver si se cumple al menos una de las condiciones para ganar
		for (Boolean i : ganaJugadorUno)
			if (i == true)
				return Estado.GANA_UNO;

		for (Boolean i : ganaJugadorDos)
			if (i == true)
				return Estado.GANA_DOS;

		// si no ganó nadie, no sé que pasó :|
		return Estado.INVALIDO;
	}

	public Estado[] ganadorDoble() {
		Mano manoUnoIzq = this.uno.getManoIzq();
		Mano manoUnoDer = this.uno.getManoDer();
		Mano manoDosIzq = this.dos.getManoIzq();
		Mano manoDosDer = this.dos.getManoDer();

		Estado estadoIzquierdas = Estado.INVALIDO;
		Estado estadoDerechas = Estado.INVALIDO;

		// verificar que las manos no sean inválidas
		if (manoUnoIzq == Mano.NADA || manoDosIzq == Mano.NADA) {
			estadoIzquierdas = Estado.INVALIDO;
		}

		if (manoUnoDer == Mano.NADA || manoDosDer == Mano.NADA) {
			estadoDerechas = Estado.INVALIDO;
		}

		// empatar si las respectivas manos son iguales
		if (manoUnoIzq == manoDosIzq) {
			estadoIzquierdas = Estado.EMPATE;
		}

		if (manoUnoDer == manoDosDer) {
			estadoDerechas = Estado.EMPATE;
		}

		Boolean[] ganaJugadorUnoIzquierdas = {
			manoUnoIzq == Mano.PAPEL && manoDosIzq == Mano.PIEDRA,
			manoUnoIzq == Mano.PIEDRA && manoDosIzq == Mano.TIJERA,
			manoUnoIzq == Mano.TIJERA && manoDosIzq == Mano.PAPEL
		};

		Boolean[] ganaJugadorDosIzquierdas = {
			manoDosIzq == Mano.PAPEL && manoUnoIzq == Mano.PIEDRA,
			manoDosIzq == Mano.PIEDRA && manoUnoIzq == Mano.TIJERA,
			manoDosIzq == Mano.TIJERA && manoUnoIzq == Mano.PAPEL
		};

		Boolean[] ganaJugadorUnoDerechas = {
			manoUnoDer == Mano.PAPEL && manoDosDer == Mano.PIEDRA,
			manoUnoDer == Mano.PIEDRA && manoDosDer == Mano.TIJERA,
			manoUnoDer == Mano.TIJERA && manoDosDer == Mano.PAPEL
		};

		Boolean[] ganaJugadorDosDerechas = {
			manoDosDer == Mano.PAPEL && manoUnoDer == Mano.PIEDRA,
			manoDosDer == Mano.PIEDRA && manoUnoDer == Mano.TIJERA,
			manoDosDer == Mano.TIJERA && manoUnoDer == Mano.PAPEL
		};

		for (Boolean i : ganaJugadorUnoIzquierdas) {
			if (i == true) {
				estadoIzquierdas = Estado.GANA_UNO;
				break;
			}
		}

		for (Boolean i : ganaJugadorDosIzquierdas) {
			if (i == true) {
				estadoIzquierdas = Estado.GANA_DOS;
				break;
			}
		}

		for (Boolean i : ganaJugadorUnoDerechas) {
			if (i == true) {
				estadoDerechas = Estado.GANA_UNO;
				break;
			}
		}

		for (Boolean i : ganaJugadorDosDerechas) {
			if (i == true) {
				estadoDerechas = Estado.GANA_DOS;
				break;
			}
		}

		// devolver el estado del juego con las manos izquierdas, y el estado
		// del juego con las manos derechas.
		Estado[] estados = new Estado[] {estadoIzquierdas, estadoDerechas};
		return estados;
	}
}
