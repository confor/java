package me.confor.tenis;

import java.util.Random;
import java.util.Scanner;

public class Juego {
	Random random = new Random();
	Jugador uno = new Jugador();
	Jugador dos = new Jugador();

	static void println(String str) {
		System.out.println(str);
	}

	public Juego() {
		println("Tenis: Contador para tie-break");
		println("Controles: a=izq, s=centro, d=der");

		int saque = 0;
		int current = 1;

		while (true) {
			// este pedazo de vómito procura que parta el jugador uno, luego
			// que cada jugador efectue dos saques.
			if (saque == 0) {
				println("Saca el jugador uno.");
				current = 1;
			} else if (saque == 1 || saque == 2) {
				println("Saca el jugador dos.");
				current = 2;
			} else if (saque == 3) {
				println("Saca el jugador uno.");
				current = 1;
			} else if (saque == 4) {
				println("Saca el jugador uno.");
				current = 1;
				saque = 0;
			}

			saque++;

			if (current == 1) {
				println("¿Jugador uno, hacia qué lado tirar?");
			} else {
				println("¿Jugador dos, hacia qué lado tirar?");
			}

			char asdf = promptChar("> ");

			if (asdf == 'a') {
				println("Tira la pelota a la izquierda.");
			} else if (asdf == 's') {
				println("Tira la pelota al centro.");
			} else if (asdf == 'd') {
				println("Tira la pelota hacia la derecha.");
			} else {
				println("chuteó la pelota pa cualquier lado :/");
			}

			int event = rand(1, 4);

			if (event == 1) {
				println("La pelota cayó dentro de la cancha.");
			} else if (event == 2) {
				if (current == 1) {
					println("La pelota cayó en la red. Jugador UNO pierde el punto.");
					dos.addPunto();
				} else {
					println("La pelota cayó en la red. Jugador DOS pierde el punto.");
					uno.addPunto();
				}
			} else if (event == 3) {
				if (current == 1) {
					println("La pelota cayó fuera de la cancha. Jugador UNO pierde el punto.");
					dos.addPunto();
				} else {
					println("La pelota cayó fuera de la cancha. Jugador DOS pierde el punto.");
					uno.addPunto();
				}
			} else if (event == 4) {
				if (current == 1) {
					println("¡Tiro ganador! Jugador UNO gana un punto.");
					uno.addPunto();
				} else {
					println("¡Tiro ganador! Jugador DOS gana un punto.");
					dos.addPunto();
				}
			}

			println("El jugador 1 tiene " + uno.getPuntos() + " puntos, y el jugador 2 tiene " + dos.getPuntos() + " puntos.");

			if (uno.getPuntos() + dos.getPuntos() > 7) {
				if (uno.getPuntos() - dos.getPuntos() == 2) {
					println("Gana el jugador uno.");
					break;
				} else if (dos.getPuntos() - uno.getPuntos() == 2) {
					println("Gana el jugador dos.");
					break;
				}
			}
		}

		return;
	}

	private int rand(int min, int max) {
		return random.nextInt(max + 1 - min) + min;
	}

	static String prompt(String prompt) {
		System.out.print(prompt);
		return new Scanner(System.in).nextLine();
	}

	static char promptChar(String prompt) {
		System.out.print(prompt);
		Scanner scanner = new Scanner(System.in);
		char str = scanner.next().charAt(0);
		return str;
	}
}
