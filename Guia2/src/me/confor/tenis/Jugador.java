package me.confor.tenis;

public class Jugador {
	private int puntos = 0;
	private int energia;

	public int getEnergia() {
		return energia;
	}

	public void setEnergia(int energia) {
		this.energia = energia;
	}

	public void addPunto() {
		this.puntos++;
	}

	public int getPuntos() {
		return puntos;
	}

	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}
}
