package race;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import race.ship.Odometer;
import race.ship.Part;
import race.ship.Ship;
import race.ship.variants.Bruja;
import race.ship.variants.CarritoDeGolf;
import race.ship.variants.Carruaje;
import race.ship.variants.Locomotora;
import race.ship.variants.Tanque;
import race.ship.variants.Velero;

public class Race {
	private Narrator narrator = new Narrator();
	private Mechanic mechanic = new Mechanic();
	private ArrayList<Ship> ships = new ArrayList<Ship>();

	private int LENGTH = 1000;

	public Race() {
		Track track = new Track();
		track.setLength(LENGTH);

		Util.p("Simulador de carrera!");

		int shipCount = 2;
		int shipVariants = 6;

		// llenar `ships` con naves al azar
		for (int i = 0; i < shipCount; i++) {
			switch(Util.randInt(1, shipVariants)) {
			case 1:
				Ship carrito = new CarritoDeGolf();
				ships.add(carrito);
				break;

			case 2:
				Ship carruaje = new Carruaje();
				ships.add(carruaje);
				break;

			case 3:
				Ship locomotora = new Locomotora();
				ships.add(locomotora);
				break;

			case 4:
				Ship bruja = new Bruja();
				ships.add(bruja);
				break;

			case 5:
				Ship velero = new Velero();
				ships.add(velero);
				break;

			case 6:
				Ship tanque = new Tanque();
				ships.add(tanque);
				break;
			}
		}

		// simular carrera
		int finished = 0;
		int t = 0; // ms
		int TIME_DIFF = 200; // ms

		Ship leader = null;
		Ship previousLeader = null;

		while(true) {
			int sec = (int) Math.floor(t / 1000);
			Util.clearScreen();

			ArrayList<Ship> shipsCopy = new ArrayList<Ship>(ships);

			Collections.sort(shipsCopy, new Comparator<Ship>() {
				@Override
				public int compare(Ship left, Ship right) {
					if (left.x() < right.x())
						return 1;
					else if (left.x() > right.x())
						return -1;
					else
						return 0;
				}
			});

			leader = shipsCopy.get(0);

			if (leader != previousLeader && leader != null) {
				if (previousLeader != null) {
					narrator.addMessage("[" + sec + "s] " + leader.name + " sobrepasa a " + previousLeader.name + "!");
				} else {
					narrator.addMessage("[" + sec + "s] " + leader.name + " lidera la carrera!");
				}

				previousLeader = leader;
			}

			if (sec > track.getLength() / 10) {
				Util.p("¡Pasó mucho tiempo! Abortando carrera.");
				break;
			}

			if (finished >= shipCount) {
				Util.p("¡Todos han finalizado la carrera! 🏁🏁🏁🏁");
				Util.ln();

				ArrayList<Ship> finishedShips = track.getFinished();
				for (int i = 0; i < finishedShips.size(); i++) {
					briefInfo(i + 1, finishedShips.get(i));
				}

				Util.ln();
				Util.p("La carrera corrió por " + sec + " segundos.");

				break;
			}

			if (t == 0) {
				for (Ship ship : ships) {
					ship.run();
				}
			}

			t += TIME_DIFF;

			Util.p("== t=" + sec + " s =======================");
			if (track.getFinished().size() > 0) {
				Util.p("Actualmente hay " + track.getFinished().size() + " naves en la meta.");
				Util.ln();
			}

			for (Ship ship : ships) {
				if (ship.x() >= track.getLength()) {
					ship.stop();
					Util.p("La nave '" + ship.name + "' alcanzó la meta");

					if (ship.done != true) {
						finished += 1;
						track.addFinished(ship);
						ship.done = true;
					}

					continue;
				} else {
					ship.tick(TIME_DIFF);
				}

				// probabilidad de 1/10 de hacer daño a una parte al azar
				if (Util.randInt(1, 10) == 1) {
					ArrayList<Part> parts = ship.getParts();
					Part randomPart = parts.get(Util.randInt(0, parts.size() - 1));

					// 1% + velocidad * 0.1%
					float randomDamage = 0.01f + ship.v() * 0.001f;
					randomPart.addHealth(-randomDamage);
				}

				info(ship);

				if (ship.getHealth() < 0.2f) {
					// por alguna razón esto hace que la posición se multiplique por dos
					ship.stop();
					narrator.addMessage("[" + sec + "s] " + "El mecanico actúa en " + ship.name + "!");
					mechanic.repair(ship);
					ship.run();
				}
			}

			if (finished == shipCount) {
				narrator.addMessage("¡Todos terminaron!");

				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				Util.p("\nSobre la carrera:");
				narrator.print(4);
			}


			try {
				Thread.sleep(TIME_DIFF);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void info(Ship ship) {
		Odometer odom = ship.getOdometer();

		Util.p(String.format("Nave: %s (hp: %.2f%%; acel: %s; vel: %s)", ship.name, ship.getHealth() * 100, odom.getAcceleration(), odom.getVelocity()));
		Util.printProgressBar((int) ship.x(), LENGTH);

		System.out.print("\n");
	}

	private void briefInfo(int position, Ship ship) {
		ArrayList<Part> parts = ship.getParts();

		Util.p("#" + position + " " + ship.name + " (" + String.format("%.4f", ship.getHealth() * 100) + "% hp)");

		System.out.print(" - partes: ");
		for (Part part : parts) {
			//Util.p(" - '" + part.name + "' (hp " + part.getHealth() * 100 + "%) y buff " + part.getBuff());
			System.out.print("'" + part.name + "' (" + String.format("%.2f", part.getHealth() * 100) + "%); ");
		}
		System.out.print("\n");
	}
}

