package race;

public class Main {
	public static void main(String[] args) {
		// ver si estamos en una terminal, para códigos ansi
		// https://stackoverflow.com/a/52365927/4301778
		if (System.console() != null && System.getenv().get("TERM") != null) {
			Util.terminal = true;
		}

		new Race();
	}
}