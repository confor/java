package race.ship;

public interface PartInterface {
	String name = "";
	float getHealth();
	void setHealth(float health);
	void addHealth(float health);
	float getBuff();
	void setBuff(float buff);
	void addRandomBuff(String[] names, Float[] buffs);
}
