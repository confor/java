package race.ship;

import race.Util;

public class Part implements PartInterface {
	public String name;
	private float health = 1f;
	private float buff = 0f;

	public Part(String name) {
		this.name = name;
	}

	@Override
	public float getHealth() {
		return this.health;
	}

	@Override
	public void setHealth(float health) {
		this.health = health;

		if (this.health < 0f) {
			this.health = 0f;
		}
	}

	@Override
	public void addHealth(float health) {
		this.health += health;

		if (this.health < 0f) {
			this.health = 0f;
		} else if (this.health > 1f) {
			this.health = 1f;
		}
	}

	@Override
	public float getBuff() {
		// buff * health
		// 10km/h * 50% hp = 5km/h
		return buff * this.getHealth();
	}

	@Override
	public void setBuff(float buff) {
		this.buff = buff;
	}

	/*
	 * es importante distinguir entre el tipo de buff que entrega esta
	 * función y la de Engine.randomFuel: aquí se *añade* un buff,
	 * mientras que en Engine.randomFuel es sumarle un porcentaje.
	 * en otras palabras:
	 *  - aquí: currentBuff + newBuff
	 *  - allá: currentBuff + currentBuff*newBuff
	 * esto es porque la guia del proyecto sugiere tener combustible que
	 * sea un *porcentaje* más rápido.
	 */
	@Override
	public void addRandomBuff(String[] names, Float[] buffs) {
		int randInt = Util.randInt(0, names.length - 1);

		String name = names[randInt];
		float buff = buffs[randInt];

		this.name += " " + name;
		// base + buff
		this.setBuff(this.getBuff() + buff);
	}
}
