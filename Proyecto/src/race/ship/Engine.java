package race.ship;

import race.Util;

public class Engine extends Part {
	public Engine(String name) {
		super(name);
	}

	public void setAccel(float accel) {
		this.setBuff(accel);
	}

	public float getAccel() {
		return this.getBuff();
	}

	public void randomFuel() {
		// 50% probabilidad de no mejorar
		if (Util.randInt(1, 2) == 1)
			return;

		String[] names = {"nuclear", "de diésel", "de biodiésel"};
		Float[] buffs =  {0.04f,     0.02f,       0.003f        };

		int randInt = Util.randInt(0, names.length - 1);

		this.name += " " + names[randInt];
		// base + base*buff
		this.setBuff(this.getBuff() + this.getBuff() * buffs[randInt]);
	}
}
