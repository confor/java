package race.ship;

public class Odometer {
	private Ship ship;

	public Odometer(Ship ship) {
		this.ship = ship;
	}

	public String getAcceleration() {
		// tiene sentido "por hora por segundo" aquí?
		return String.format("%.2f", this.ship.getAccel()) + " km/hs";
	}

	public String getVelocity() {
		return String.format("%.2f", this.ship.v()) + " km/h";
	}

	public String getPosition() {
		return String.format("%.2f", this.ship.x()) + " km";
	}
}
