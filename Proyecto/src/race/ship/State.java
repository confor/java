package race.ship;

public enum State {
	STOPPED,
	RUNNING
}