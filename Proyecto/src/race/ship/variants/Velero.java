package race.ship.variants;

import race.Util;
import race.ship.Engine;
import race.ship.Part;
import race.ship.Ship;

public class Velero extends Ship {
	public Velero() {
		super("Velero");

		Part barco = new Part("Barco");
		Part aparejo = new Part("Aparejo");
		Engine vela = new Engine("Vela");
		vela.setAccel(10f);

		this.addPart(barco);
		this.addPart(aparejo);
		this.addPart(vela);

		// 50% probabilidad de mejorar al azar
		if (Util.randInt(1, 2) == 1) {
			String[] names = {"cuadra", "de cuchillo"};
			Float[] buffs =  { 0.01f,    0.01f       };

			vela.addRandomBuff(names, buffs);
		}
	}
}
