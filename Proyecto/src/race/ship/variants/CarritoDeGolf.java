package race.ship.variants;

import race.ship.Engine;
import race.ship.Part;
import race.ship.Ship;

public class CarritoDeGolf extends Ship {
	public CarritoDeGolf() {
		super("Carrito de golf");

		Part cuerpo = new Part("Carrocería");
		Part ruedas = new Part("Ruedas");
		Engine motor = new Engine("Motor");
		motor.setAccel(10f);
		motor.randomFuel();

		this.addPart(cuerpo);
		this.addPart(ruedas);
		this.addPart(motor);
	}
}
