package race.ship.variants;

import race.Util;
import race.ship.Engine;
import race.ship.Part;
import race.ship.Ship;

public class Carruaje extends Ship {
	public Carruaje() {
		super("Carruaje de caballos");

		Part cuerpo = new Part("Carruaje");
		Part ruedas = new Part("Ruedas");
		Engine motor = new Engine("Caballos");
		motor.setAccel(10f);

		this.addPart(cuerpo);
		this.addPart(ruedas);
		this.addPart(motor);

		// 20% probabilidad de mejorar al azar
		if (Util.randInt(1, 4) == 1) {
			String[] names = {"reales", "de carrera", "de raza"};
			Float[] buffs =  { 0.04f,    0.03f,        0.02f   };

			motor.addRandomBuff(names, buffs);
		}
	}
}
