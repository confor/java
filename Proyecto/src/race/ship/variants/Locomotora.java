package race.ship.variants;

import race.ship.Engine;
import race.ship.Part;
import race.ship.Ship;

public class Locomotora extends Ship {
	public Locomotora() {
		super("Locomotora");

		Part locomotora = new Part("Locomotora");
		Part ruedas = new Part("Ruedas");
		Engine motor = new Engine("Motor");
		motor.setAccel(10f);
		motor.randomFuel();

		this.addPart(locomotora);
		this.addPart(ruedas);
		this.addPart(motor);
	}
}
