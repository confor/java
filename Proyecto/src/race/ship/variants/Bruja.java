package race.ship.variants;

import race.Util;
import race.ship.Engine;
import race.ship.Part;
import race.ship.Ship;

public class Bruja extends Ship {
	public Bruja() {
		super("Bruja");

		Part bruja = new Part("Bruja");
		//Part escoba = new Part("Escoba");
		Engine motor = new Engine("Escoba");
		motor.setAccel(10f);

		this.addPart(bruja);
		//this.addPart(escoba);
		this.addPart(motor);

		// 50% probabilidad de mejorar al azar
		if (Util.randInt(1, 2) == 1) {
			String[] names = {"de roble", "de metal"};
			Float[] buffs =  { 0.04f,       0.02f   };

			motor.addRandomBuff(names, buffs);
		}
	}
}
