package race.ship.variants;

import race.ship.Engine;
import race.ship.Part;
import race.ship.Ship;

public class Tanque extends Ship {
	public Tanque() {
		super("Tanque");

		Part cuerpo = new Part("Tanque");
		Part blindaje = new Part("Blindaje");
		Engine motor = new Engine("Motor");
		motor.setAccel(10f);
		motor.randomFuel();

		this.addPart(cuerpo);
		this.addPart(blindaje);
		this.addPart(motor);
	}
}
