package race.ship;

import java.util.ArrayList;

public interface ShipInterface {
	State state = State.STOPPED;
	boolean done = false;

	String name = "";
	void stop();
	void run();
	void tick(float ms);
	void addPart(Part part);
	ArrayList<Part> getParts();
	float getHealth();
	float getAccel();
	float v();
	float x();
	Odometer getOdometer();
	void setOdometer(Odometer odometer);
	State getState();
	void setState(State state);
}
