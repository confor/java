package race.ship;

import java.util.ArrayList;

public class Ship implements ShipInterface {
	private State state = State.STOPPED;
	public boolean done = false; // TODO BORRAR PARCHE RÁPIDO

	private float x0 = 0f; // pos inicial
	private float t = 0f; // tiempo que lleva acelerando (miliseg)

	public String name;
	private ArrayList<Part> parts = new ArrayList<Part>();
	private Odometer odometer;

	public Ship(String name) {
		this.name = name;
		this.setOdometer(new Odometer(this));
	}

	@Override
	public void stop() {
		this.x0 += this.x();
		this.t = 0;
		this.setState(State.STOPPED);
	}

	@Override
	public void run() {
		this.setState(State.RUNNING);
	}

	@Override
	public void tick(float ms) {
		if (this.getState() == State.RUNNING) {
			this.t += ms;
		}
	}

	@Override
	public void addPart(Part part) {
		this.parts.add(part);
	}

	@Override
	public ArrayList<Part> getParts() {
		return this.parts;
	}

	@Override
	public float getHealth() {
		float health = 1f;

		for (Part part : this.parts) {
			health -= (1f - part.getHealth()); // :s
		}

		if (health > 1f) {
			health = 1f;
		} else if (health < 0f) {
			health = 0f;
		}

		return health;
	}

	@Override
	public float getAccel() {
		float accel = 0f;

		// esto hace variar la aceleración. las fórmulas no
		// funcionan para aceleración *no* constante.
		for (Part part : this.parts) {
			accel += part.getBuff();
		}

		return accel;
	}

	@Override
	public float v() {
		float a0 = this.getAccel();
		float t = this.t / 1000; // ms -> s

		// FIXME esto puede ser negativo...
		return a0 * t;
	}

	@Override
	public float x() {
		float x0 = this.x0;
		float t = this.t / 1000; // ms -> s
		float a0 = this.getAccel();

		// FIXME la nave puede devolverse! D:
		return x0 + (a0 / 2) * (t * t);
	}

	@Override
	public Odometer getOdometer() {
		return odometer;
	}

	@Override
	public void setOdometer(Odometer odometer) {
		this.odometer = odometer;
	}

	@Override
	public State getState() {
		return this.state;
	}

	@Override
	public void setState(State state) {
		this.state = state;
	}
}
