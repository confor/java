package race;

import java.util.ArrayList;
import java.util.Collections;

public class Narrator {
	private ArrayList<String> messageQueue = new ArrayList<String>();

	public void addMessage(String msg) {
		messageQueue.add(msg);
	}

	public void print(int limit) {
		ArrayList<String> msgQueueCopy = new ArrayList<String>(messageQueue);
		Collections.reverse(msgQueueCopy);

		for (int i = 0; i < msgQueueCopy.size(); i++) {
			Util.p("- " + msgQueueCopy.get(i));

			if (i >= limit)
				return;
		}
	}
}
