package race;

import java.util.ArrayList;

import race.ship.Ship;

public class Track {
	private int length = 10000;
	private ArrayList<Ship> finished = new ArrayList<Ship>();

	// TODO variantes de pista?
	// en circulo (varias vueltas)
	// línea recta

	// TODO obstáculos
	// - posibilidad al azar (<10%?) de aparecer y detener un vehiculo

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public ArrayList<Ship> getFinished() {
		return finished;
	}

	public void setFinished(ArrayList<Ship> finished) {
		this.finished = finished;
	}

	public void addFinished(Ship ship) {
		this.finished.add(ship);
	}
}
