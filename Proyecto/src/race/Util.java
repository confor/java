package race;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Util {
	public static boolean terminal = false;

	public static int randInt(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}

	public static void ln() {
		System.out.println();
	}

	public static void p(String str) {
		System.out.println(str);
	}

	public static int promptInt() {
		Scanner scanner = new Scanner(System.in);
		return scanner.nextInt();
	}

	// ansi probado con bash, con cmd.exe y la consola de eclipse
	public static void clearScreen() {
		if (Util.terminal) {
			System.out.print("\033[2J");
		} else {
			for(int i = 0; i < 4; i++) {
				System.out.println();
			}
		}
	}

	// imprime una barra de progreso de 102 caracteres de ancho
	public static void printProgressBar(float progress, float total) {
		float percent = progress / total * 100;
		String bar = "";

		for (int i = 0; i < 100; i++) {
			if (i <= percent) {
				bar += "#";
			} else {
				bar += ".";
			}
		}

		System.out.print("[" + bar + "]");
	}
}
