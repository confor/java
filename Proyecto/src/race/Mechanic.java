package race;

import java.util.ArrayList;

import race.ship.Part;
import race.ship.Ship;

public class Mechanic {
	public boolean busy = false; // ¿está ocupado reparando algo?

	public void repair(Ship ship) {
		ArrayList<Part> parts = ship.getParts();

		for (Part part : parts) {
			if (part.getHealth() > 0.9f) {
				continue;
			}

			// +50% vida a UNA parte
			part.addHealth(0.5f);
			break;
		}
	}
}
