#!/bin/bash

echo 'Simulador de carrera!'

sleep 1
clear

echo '== t=0 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 2,00 km/h)'
echo '[#...................................................................................................]'
echo 'Nave: Velero (hp: 100,00%; acel: 10,00 km/hs; vel: 2,00 km/h)'
echo '[#...................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=0 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 4,00 km/h)'
echo '[#...................................................................................................]'
echo 'Nave: Velero (hp: 100,00%; acel: 10,00 km/hs; vel: 4,00 km/h)'
echo '[#...................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=0 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 6,00 km/h)'
echo '[#...................................................................................................]'
echo 'Nave: Velero (hp: 100,00%; acel: 10,00 km/hs; vel: 6,00 km/h)'
echo '[#...................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=0 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 8,00 km/h)'
echo '[#...................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 7,86 km/h)'
echo '[#...................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=0 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 10,00 km/h)'
echo '[#...................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 9,82 km/h)'
echo '[#...................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=1 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 12,00 km/h)'
echo '[#...................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 11,78 km/h)'
echo '[#...................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=1 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 14,00 km/h)'
echo '[#...................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 13,75 km/h)'
echo '[#...................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=1 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 16,00 km/h)'
echo '[#...................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 15,71 km/h)'
echo '[#...................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=1 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 18,00 km/h)'
echo '[##..................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 17,68 km/h)'
echo '[##..................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=1 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 20,00 km/h)'
echo '[##..................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 19,64 km/h)'
echo '[##..................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=2 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 22,00 km/h)'
echo '[##..................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 21,60 km/h)'
echo '[##..................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=2 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 24,00 km/h)'
echo '[###.................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 23,57 km/h)'
echo '[###.................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=2 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 26,00 km/h)'
echo '[###.................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 25,53 km/h)'
echo '[###.................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=2 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 28,00 km/h)'
echo '[###.................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 27,50 km/h)'
echo '[###.................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=2 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 30,00 km/h)'
echo '[####................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 29,46 km/h)'
echo '[####................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=3 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 32,00 km/h)'
echo '[####................................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 31,42 km/h)'
echo '[####................................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=3 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 34,00 km/h)'
echo '[#####...............................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 33,39 km/h)'
echo '[#####...............................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=3 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 36,00 km/h)'
echo '[#####...............................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 35,35 km/h)'
echo '[#####...............................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=3 s ======================='
echo 'Nave: Bruja (hp: 100,00%; acel: 10,00 km/hs; vel: 38,00 km/h)'
echo '[######..............................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 37,32 km/h)'
echo '[######..............................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=3 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 38,00 km/h)'
echo '[######..............................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 39,28 km/h)'
echo '[######..............................................................................................]'

echo 'Sobre la carrera:'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=4 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 39,90 km/h)'
echo '[######..............................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 41,24 km/h)'
echo '[#######.............................................................................................]'

echo 'Sobre la carrera:'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=4 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 41,80 km/h)'
echo '[#######.............................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 43,21 km/h)'
echo '[#######.............................................................................................]'

echo 'Sobre la carrera:'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=4 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 43,70 km/h)'
echo '[########............................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 45,17 km/h)'
echo '[########............................................................................................]'

echo 'Sobre la carrera:'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=4 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 45,60 km/h)'
echo '[########............................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 47,14 km/h)'
echo '[#########...........................................................................................]'

echo 'Sobre la carrera:'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=4 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 47,50 km/h)'
echo '[#########...........................................................................................]'
echo 'Nave: Velero (hp: 98,20%; acel: 9,82 km/hs; vel: 49,10 km/h)'
echo '[#########...........................................................................................]'

echo 'Sobre la carrera:'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=5 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 49,40 km/h)'
echo '[##########..........................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 47,89 km/h)'
echo '[#########...........................................................................................]'

echo 'Sobre la carrera:'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=5 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 51,30 km/h)'
echo '[##########..........................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 49,73 km/h)'
echo '[##########..........................................................................................]'

echo 'Sobre la carrera:'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=5 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 53,20 km/h)'
echo '[###########.........................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 51,57 km/h)'
echo '[###########.........................................................................................]'

echo 'Sobre la carrera:'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=5 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 55,10 km/h)'
echo '[############........................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 53,41 km/h)'
echo '[############........................................................................................]'

echo 'Sobre la carrera:'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=5 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 57,00 km/h)'
echo '[#############.......................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 55,26 km/h)'
echo '[############........................................................................................]'

echo 'Sobre la carrera:'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=6 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 58,90 km/h)'
echo '[##############......................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 57,10 km/h)'
echo '[#############.......................................................................................]'

echo 'Sobre la carrera:'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=6 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 60,80 km/h)'
echo '[##############......................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 58,94 km/h)'
echo '[##############......................................................................................]'

echo 'Sobre la carrera:'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=6 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 62,70 km/h)'
echo '[###############.....................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 60,78 km/h)'
echo '[###############.....................................................................................]'

echo 'Sobre la carrera:'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=6 s ======================='
echo 'Nave: Bruja (hp: 95,00%; acel: 9,50 km/hs; vel: 64,60 km/h)'
echo '[################....................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 62,62 km/h)'
echo '[################....................................................................................]'

echo 'Sobre la carrera:'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=6 s ======================='
echo 'Nave: Bruja (hp: 87,35%; acel: 8,73 km/hs; vel: 61,14 km/h)'
echo '[################....................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 64,47 km/h)'
echo '[#################...................................................................................]'

echo 'Sobre la carrera:'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=7 s ======================='
echo 'Nave: Bruja (hp: 80,06%; acel: 8,01 km/hs; vel: 57,64 km/h)'
echo '[###############.....................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 66,31 km/h)'
echo '[##################..................................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=7 s ======================='
echo 'Nave: Bruja (hp: 80,06%; acel: 8,01 km/hs; vel: 59,24 km/h)'
echo '[################....................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 68,15 km/h)'
echo '[###################.................................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=7 s ======================='
echo 'Nave: Bruja (hp: 80,06%; acel: 8,01 km/hs; vel: 60,85 km/h)'
echo '[#################...................................................................................]'
echo 'Nave: Velero (hp: 92,09%; acel: 9,21 km/hs; vel: 69,99 km/h)'
echo '[###################.................................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=7 s ======================='
echo 'Nave: Bruja (hp: 80,06%; acel: 8,01 km/hs; vel: 62,45 km/h)'
echo '[##################..................................................................................]'
echo 'Nave: Velero (hp: 83,91%; acel: 9,21 km/hs; vel: 71,83 km/h)'
echo '[#####################...............................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=7 s ======================='
echo 'Nave: Bruja (hp: 80,06%; acel: 8,01 km/hs; vel: 64,05 km/h)'
echo '[###################.................................................................................]'
echo 'Nave: Velero (hp: 83,91%; acel: 9,21 km/hs; vel: 73,67 km/h)'
echo '[######################..............................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=8 s ======================='
echo 'Nave: Bruja (hp: 80,06%; acel: 8,01 km/hs; vel: 65,65 km/h)'
echo '[####################................................................................................]'
echo 'Nave: Velero (hp: 83,91%; acel: 9,21 km/hs; vel: 75,52 km/h)'
echo '[#######################.............................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=8 s ======================='
echo 'Nave: Bruja (hp: 80,06%; acel: 8,01 km/hs; vel: 67,25 km/h)'
echo '[#####################...............................................................................]'
echo 'Nave: Velero (hp: 83,91%; acel: 9,21 km/hs; vel: 77,36 km/h)'
echo '[########################............................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=8 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 62,07 km/h)'
echo '[####################................................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 79,20 km/h)'
echo '[#########################...........................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=8 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 63,51 km/h)'
echo '[####################................................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 81,04 km/h)'
echo '[##########################..........................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=8 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 64,96 km/h)'
echo '[#####################...............................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 82,88 km/h)'
echo '[###########################.........................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=9 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 66,40 km/h)'
echo '[######################..............................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 84,73 km/h)'
echo '[############################........................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=9 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 67,85 km/h)'
echo '[#######################.............................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 86,57 km/h)'
echo '[##############################......................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=9 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 69,29 km/h)'
echo '[########################............................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 88,41 km/h)'
echo '[###############################.....................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=9 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 70,73 km/h)'
echo '[#########################...........................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 90,25 km/h)'
echo '[################################....................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=9 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 72,18 km/h)'
echo '[##########################..........................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 92,09 km/h)'
echo '[#################################...................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=10 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 73,62 km/h)'
echo '[###########################.........................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 93,94 km/h)'
echo '[###################################.................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=10 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 75,06 km/h)'
echo '[############################........................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 95,78 km/h)'
echo '[####################################................................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=10 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 76,51 km/h)'
echo '[#############################.......................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 97,62 km/h)'
echo '[#####################################...............................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=10 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 77,95 km/h)'
echo '[###############################.....................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 99,46 km/h)'
echo '[#######################################.............................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=10 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 79,39 km/h)'
echo '[################################....................................................................]'
echo 'Nave: Velero (hp: 74,99%; acel: 9,21 km/hs; vel: 101,30 km/h)'
echo '[########################################............................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=11 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 80,84 km/h)'
echo '[#################################...................................................................]'
echo 'Nave: Velero (hp: 63,68%; acel: 8,08 km/hs; vel: 90,47 km/h)'
echo '[#####################################...............................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=11 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 82,28 km/h)'
echo '[##################################..................................................................]'
echo 'Nave: Velero (hp: 63,68%; acel: 8,08 km/hs; vel: 92,09 km/h)'
echo '[######################################..............................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=11 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 83,72 km/h)'
echo '[###################################.................................................................]'
echo 'Nave: Velero (hp: 63,68%; acel: 8,08 km/hs; vel: 93,70 km/h)'
echo '[#######################################.............................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=11 s ======================='
echo 'Nave: Bruja (hp: 72,18%; acel: 7,22 km/hs; vel: 85,17 km/h)'
echo '[####################################................................................................]'
echo 'Nave: Velero (hp: 63,68%; acel: 8,08 km/hs; vel: 95,32 km/h)'
echo '[#########################################...........................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=11 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 86,61 km/h)'
echo '[######################################..............................................................]'
echo 'Nave: Velero (hp: 63,68%; acel: 8,08 km/hs; vel: 96,93 km/h)'
echo '[##########################################..........................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=12 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 88,05 km/h)'
echo '[#######################################.............................................................]'
echo 'Nave: Velero (hp: 63,68%; acel: 8,08 km/hs; vel: 98,55 km/h)'
echo '[###########################################.........................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=12 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 89,50 km/h)'
echo '[########################################............................................................]'
echo 'Nave: Velero (hp: 63,68%; acel: 8,08 km/hs; vel: 100,17 km/h)'
echo '[#############################################.......................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=12 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 90,94 km/h)'
echo '[#########################################...........................................................]'
echo 'Nave: Velero (hp: 63,68%; acel: 8,08 km/hs; vel: 101,78 km/h)'
echo '[##############################################......................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=12 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 92,38 km/h)'
echo '[###########################################.........................................................]'
echo 'Nave: Velero (hp: 63,68%; acel: 8,08 km/hs; vel: 103,40 km/h)'
echo '[################################################....................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=12 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 93,83 km/h)'
echo '[############################################........................................................]'
echo 'Nave: Velero (hp: 63,68%; acel: 8,08 km/hs; vel: 105,01 km/h)'
echo '[#################################################...................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=13 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 95,27 km/h)'
echo '[#############################################.......................................................]'
echo 'Nave: Velero (hp: 52,01%; acel: 6,91 km/hs; vel: 91,23 km/h)'
echo '[############################################........................................................]'

echo 'Sobre la carrera:'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=13 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 96,72 km/h)'
echo '[###############################################.....................................................]'
echo 'Nave: Velero (hp: 52,01%; acel: 6,91 km/hs; vel: 92,62 km/h)'
echo '[#############################################.......................................................]'

echo 'Sobre la carrera:'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=13 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 98,16 km/h)'
echo '[################################################....................................................]'
echo 'Nave: Velero (hp: 52,01%; acel: 6,91 km/hs; vel: 94,00 km/h)'
echo '[##############################################......................................................]'

echo 'Sobre la carrera:'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=13 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 99,60 km/h)'
echo '[##################################################..................................................]'
echo 'Nave: Velero (hp: 52,01%; acel: 6,91 km/hs; vel: 95,38 km/h)'
echo '[################################################....................................................]'

echo 'Sobre la carrera:'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=13 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 101,05 km/h)'
echo '[###################################################.................................................]'
echo 'Nave: Velero (hp: 52,01%; acel: 6,91 km/hs; vel: 96,76 km/h)'
echo '[#################################################...................................................]'

echo 'Sobre la carrera:'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=14 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 102,49 km/h)'
echo '[####################################################................................................]'
echo 'Nave: Velero (hp: 52,01%; acel: 6,91 km/hs; vel: 98,15 km/h)'
echo '[##################################################..................................................]'

echo 'Sobre la carrera:'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=14 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 103,93 km/h)'
echo '[######################################################..............................................]'
echo 'Nave: Velero (hp: 52,01%; acel: 6,91 km/hs; vel: 99,53 km/h)'
echo '[####################################################................................................]'

echo 'Sobre la carrera:'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=14 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 105,38 km/h)'
echo '[#######################################################.............................................]'
echo 'Nave: Velero (hp: 52,01%; acel: 6,91 km/hs; vel: 100,91 km/h)'
echo '[#####################################################...............................................]'

echo 'Sobre la carrera:'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=14 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 106,82 km/h)'
echo '[#########################################################...........................................]'
echo 'Nave: Velero (hp: 40,78%; acel: 6,91 km/hs; vel: 102,29 km/h)'
echo '[#######################################################.............................................]'

echo 'Sobre la carrera:'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=14 s ======================='
echo 'Nave: Bruja (hp: 62,51%; acel: 7,22 km/hs; vel: 108,26 km/h)'
echo '[##########################################################..........................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 103,67 km/h)'
echo '[########################################################............................................]'

echo 'Sobre la carrera:'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=15 s ======================='
echo 'Nave: Bruja (hp: 50,54%; acel: 6,02 km/hs; vel: 91,51 km/h)'
echo '[##################################################..................................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 105,06 km/h)'
echo '[##########################################################..........................................]'

echo 'Sobre la carrera:'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'
echo '- [0s] Bruja lidera la carrera!'

sleep 0.2
clear

echo '== t=15 s ======================='
echo 'Nave: Bruja (hp: 50,54%; acel: 6,02 km/hs; vel: 92,72 km/h)'
echo '[###################################################.................................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 106,44 km/h)'
echo '[###########################################################.........................................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=15 s ======================='
echo 'Nave: Bruja (hp: 50,54%; acel: 6,02 km/hs; vel: 93,92 km/h)'
echo '[#####################################################...............................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 107,82 km/h)'
echo '[#############################################################.......................................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=15 s ======================='
echo 'Nave: Bruja (hp: 50,54%; acel: 6,02 km/hs; vel: 95,12 km/h)'
echo '[######################################################..............................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 109,20 km/h)'
echo '[##############################################################......................................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=15 s ======================='
echo 'Nave: Bruja (hp: 50,54%; acel: 6,02 km/hs; vel: 96,33 km/h)'
echo '[########################################################............................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 110,59 km/h)'
echo '[################################################################....................................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=16 s ======================='
echo 'Nave: Bruja (hp: 50,54%; acel: 6,02 km/hs; vel: 97,53 km/h)'
echo '[#########################################################...........................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 111,97 km/h)'
echo '[#################################################################...................................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=16 s ======================='
echo 'Nave: Bruja (hp: 50,54%; acel: 6,02 km/hs; vel: 98,74 km/h)'
echo '[##########################################################..........................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 113,35 km/h)'
echo '[###################################################################.................................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=16 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 99,94 km/h)'
echo '[############################################################........................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 114,73 km/h)'
echo '[#####################################################################...............................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=16 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 101,14 km/h)'
echo '[#############################################################.......................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 116,12 km/h)'
echo '[######################################################################..............................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=16 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 102,35 km/h)'
echo '[###############################################################.....................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 117,50 km/h)'
echo '[########################################################################............................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=17 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 103,55 km/h)'
echo '[################################################################....................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 118,88 km/h)'
echo '[##########################################################################..........................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=17 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 104,76 km/h)'
echo '[##################################################################..................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 120,26 km/h)'
echo '[###########################################################################.........................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=17 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 105,96 km/h)'
echo '[###################################################################.................................]'
echo 'Nave: Velero (hp: 29,42%; acel: 6,91 km/hs; vel: 121,64 km/h)'
echo '[#############################################################################.......................]'

echo 'Sobre la carrera:'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'
echo '- [4s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=17 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 107,16 km/h)'
echo '[#####################################################################...............................]'
echo 'Nave: Velero (hp: 16,11%; acel: 6,91 km/hs; vel: 123,03 km/h)'
echo '[###############################################################################.....................]'

echo 'Sobre la carrera:'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'

sleep 0.2
clear

echo '== t=17 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 108,37 km/h)'
echo '[######################################################################..............................]'
echo 'Nave: Velero (hp: 60,93%; acel: 6,91 km/hs; vel: 1,38 km/h)'
echo '[###############################################################################.....................]'

echo 'Sobre la carrera:'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'

sleep 0.2
clear

echo '== t=18 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 109,57 km/h)'
echo '[########################################################################............................]'
echo 'Nave: Velero (hp: 60,93%; acel: 6,91 km/hs; vel: 2,76 km/h)'
echo '[###############################################################################.....................]'

echo 'Sobre la carrera:'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'

sleep 0.2
clear

echo '== t=18 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 110,78 km/h)'
echo '[#########################################################################...........................]'
echo 'Nave: Velero (hp: 60,93%; acel: 6,91 km/hs; vel: 4,15 km/h)'
echo '[###############################################################################.....................]'

echo 'Sobre la carrera:'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'

sleep 0.2
clear

echo '== t=18 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 111,98 km/h)'
echo '[###########################################################################.........................]'
echo 'Nave: Velero (hp: 60,93%; acel: 6,91 km/hs; vel: 5,53 km/h)'
echo '[###############################################################################.....................]'

echo 'Sobre la carrera:'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'

sleep 0.2
clear

echo '== t=18 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 113,19 km/h)'
echo '[############################################################################........................]'
echo 'Nave: Velero (hp: 60,93%; acel: 6,91 km/hs; vel: 6,91 km/h)'
echo '[###############################################################################.....................]'

echo 'Sobre la carrera:'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'

sleep 0.2
clear

echo '== t=18 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 114,39 km/h)'
echo '[##############################################################################......................]'
echo 'Nave: Velero (hp: 60,93%; acel: 6,91 km/hs; vel: 8,29 km/h)'
echo '[###############################################################################.....................]'

echo 'Sobre la carrera:'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'

sleep 0.2
clear

echo '== t=19 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 115,59 km/h)'
echo '[################################################################################....................]'
echo 'Nave: Velero (hp: 60,93%; acel: 6,91 km/hs; vel: 9,68 km/h)'
echo '[###############################################################################.....................]'

echo 'Sobre la carrera:'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'
echo '- [5s] Bruja sobrepasa a Velero!'

sleep 0.2
clear

echo '== t=19 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 116,80 km/h)'
echo '[#################################################################################...................]'
echo 'Nave: Velero (hp: 60,93%; acel: 6,91 km/hs; vel: 11,06 km/h)'
echo '[###############################################################################.....................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=19 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 118,00 km/h)'
echo '[###################################################################################.................]'
echo 'Nave: Velero (hp: 58,69%; acel: 6,91 km/hs; vel: 12,44 km/h)'
echo '[################################################################################....................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=19 s ======================='
echo 'Nave: Bruja (hp: 39,55%; acel: 6,02 km/hs; vel: 119,21 km/h)'
echo '[#####################################################################################...............]'
echo 'Nave: Velero (hp: 58,69%; acel: 6,91 km/hs; vel: 13,82 km/h)'
echo '[################################################################################....................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=19 s ======================='
echo 'Nave: Bruja (hp: 26,51%; acel: 6,02 km/hs; vel: 120,41 km/h)'
echo '[#######################################################################################.............]'
echo 'Nave: Velero (hp: 58,69%; acel: 6,91 km/hs; vel: 15,21 km/h)'
echo '[################################################################################....................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=20 s ======================='
echo 'Nave: Bruja (hp: 26,51%; acel: 6,02 km/hs; vel: 121,61 km/h)'
echo '[########################################################################################............]'
echo 'Nave: Velero (hp: 58,69%; acel: 6,91 km/hs; vel: 16,59 km/h)'
echo '[################################################################################....................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=20 s ======================='
echo 'Nave: Bruja (hp: 26,51%; acel: 6,02 km/hs; vel: 122,82 km/h)'
echo '[##########################################################################################..........]'
echo 'Nave: Velero (hp: 58,69%; acel: 6,91 km/hs; vel: 17,97 km/h)'
echo '[################################################################################....................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=20 s ======================='
echo 'Nave: Bruja (hp: 26,51%; acel: 6,02 km/hs; vel: 124,02 km/h)'
echo '[############################################################################################........]'
echo 'Nave: Velero (hp: 55,75%; acel: 6,62 km/hs; vel: 18,53 km/h)'
echo '[#################################################################################...................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=20 s ======================='
echo 'Nave: Bruja (hp: 26,51%; acel: 6,02 km/hs; vel: 125,23 km/h)'
echo '[##############################################################################################......]'
echo 'Nave: Velero (hp: 55,75%; acel: 6,62 km/hs; vel: 19,85 km/h)'
echo '[#################################################################################...................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=20 s ======================='
echo 'Nave: Bruja (hp: 26,51%; acel: 6,02 km/hs; vel: 126,43 km/h)'
echo '[###############################################################################################.....]'
echo 'Nave: Velero (hp: 55,75%; acel: 6,62 km/hs; vel: 21,18 km/h)'
echo '[#################################################################################...................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=21 s ======================='
echo 'Nave: Bruja (hp: 26,51%; acel: 6,02 km/hs; vel: 127,63 km/h)'
echo '[#################################################################################################...]'
echo 'Nave: Velero (hp: 55,75%; acel: 6,62 km/hs; vel: 22,50 km/h)'
echo '[#################################################################################...................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=21 s ======================='
echo 'Nave: Bruja (hp: 26,51%; acel: 6,02 km/hs; vel: 128,84 km/h)'
echo '[###################################################################################################.]'
echo 'Nave: Velero (hp: 55,75%; acel: 6,62 km/hs; vel: 23,83 km/h)'
echo '[##################################################################################..................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=21 s ======================='
echo 'Nave: Bruja (hp: 26,51%; acel: 6,02 km/hs; vel: 130,04 km/h)'
echo '[####################################################################################################]'
echo 'Nave: Velero (hp: 55,75%; acel: 6,62 km/hs; vel: 25,15 km/h)'
echo '[##################################################################################..................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=21 s ======================='
echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 55,75%; acel: 6,62 km/hs; vel: 26,47 km/h)'
echo '[##################################################################################..................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=21 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 55,75%; acel: 6,62 km/hs; vel: 27,80 km/h)'
echo '[###################################################################################.................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=22 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 55,75%; acel: 6,62 km/hs; vel: 29,12 km/h)'
echo '[###################################################################################.................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=22 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 51,71%; acel: 6,21 km/hs; vel: 28,58 km/h)'
echo '[###################################################################################.................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=22 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 51,71%; acel: 6,21 km/hs; vel: 29,83 km/h)'
echo '[####################################################################################................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=22 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 51,71%; acel: 6,21 km/hs; vel: 31,07 km/h)'
echo '[####################################################################################................]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=22 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 51,71%; acel: 6,21 km/hs; vel: 32,31 km/h)'
echo '[#####################################################################################...............]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=23 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 31,20 km/h)'
echo '[#####################################################################################...............]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=23 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 32,36 km/h)'
echo '[#####################################################################################...............]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=23 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 33,51 km/h)'
echo '[######################################################################################..............]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=23 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 34,67 km/h)'
echo '[######################################################################################..............]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=23 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 35,82 km/h)'
echo '[#######################################################################################.............]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=24 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 36,98 km/h)'
echo '[#######################################################################################.............]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=24 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 38,14 km/h)'
echo '[########################################################################################............]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=24 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 39,29 km/h)'
echo '[########################################################################################............]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=24 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 40,45 km/h)'
echo '[#########################################################################################...........]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=24 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 41,60 km/h)'
echo '[#########################################################################################...........]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=25 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 42,76 km/h)'
echo '[##########################################################################################..........]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=25 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 43,91 km/h)'
echo '[###########################################################################################.........]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=25 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 45,07 km/h)'
echo '[###########################################################################################.........]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=25 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 46,23 km/h)'
echo '[############################################################################################........]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=25 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 47,38 km/h)'
echo '[#############################################################################################.......]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=26 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 48,54 km/h)'
echo '[#############################################################################################.......]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=26 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 49,69 km/h)'
echo '[##############################################################################################......]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=26 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 50,85 km/h)'
echo '[###############################################################################################.....]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=26 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 52,00 km/h)'
echo '[###############################################################################################.....]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=26 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 47,35%; acel: 5,78 km/hs; vel: 53,16 km/h)'
echo '[################################################################################################....]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=27 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 40,92%; acel: 5,78 km/hs; vel: 54,31 km/h)'
echo '[#################################################################################################...]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=27 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 40,92%; acel: 5,78 km/hs; vel: 55,47 km/h)'
echo '[##################################################################################################..]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=27 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 40,92%; acel: 5,78 km/hs; vel: 56,63 km/h)'
echo '[###################################################################################################.]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=27 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 40,92%; acel: 5,78 km/hs; vel: 57,78 km/h)'
echo '[###################################################################################################.]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=27 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 40,92%; acel: 5,78 km/hs; vel: 58,94 km/h)'
echo '[####################################################################################################]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=28 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'Nave: Velero (hp: 40,92%; acel: 5,78 km/hs; vel: 60,09 km/h)'
echo '[####################################################################################################]'

echo 'Sobre la carrera:'
echo '- [19s] Bruja sobrepasa a Velero!'
echo '- [17s] El mecanico actúa en Velero!'
echo '- [15s] Velero sobrepasa a Bruja!'
echo '- [13s] Bruja sobrepasa a Velero!'
echo '- [7s] Velero sobrepasa a Bruja!'

sleep 0.2
clear

echo '== t=28 s ======================='
echo 'Actualmente hay 1 naves en la meta.'

echo 'La nave Bruja alcanzó la meta'
echo 'La nave Velero alcanzó la meta'

sleep 0.2
clear

echo '¡Todos han finalizado la carrera! 🏁🏁🏁🏁'

echo '#1 Bruja (26,5088% hp)'
echo ' - partes: Bruja (66,30%); Escoba (60,20%); '
echo '#2 Velero (40,9225% hp)'
echo ' - partes: Barco (100,00%); Aparejo (83,14%); Vela (57,78%); '

echo 'La carrera corrió por 28 segundos.'
