Guía de práctica I
==================

Esta carpeta contiene mi solución a la primera guía de programación avanzada,
"clases y objetos", en Java 8.

No recuerdo si hice la guía completa, y no era evaluada.

1. Implemente la clase "Tarjeta" que permita manipular la siguiente información:
- Nombre del titular
- Saldo de la tarjeta
Además, un método que actualice el saldo de la tarjeta producto de una compra.

2. Cree una clase "Libro" que guarde el título, autor, número total de ejemplares
del libro y número de ejemplares prestados. La clase debe tener:
- Constructor por defecto
- Constructor con parámetros
- setter/getter para atributos privados

3. Algoritmo de Natalia para encriptar números. Dado un número de 4 dígitos:
- Reemplazar cada dígito por la sumatoria de su dígito hasta su posición módulo 10
    - ejemplo: 1234 => (1), (1+2), (1+2+3), (1+2+3+4) = 1, 3, 6, 0
- Sumar 7 a cada dígito
- Intercambiar el primer dígito con el tercero, y el segundo con el cuarto.
- Imprimir el resultado.

4. Pepe y sus amigos buscan saber cuánto paga cada uno en una cuenta de un restaurante
ficticio. Acostumbrban a pagar en partes iguales, pero la cuenta no considera el IVA ni
la propina (19% y 10%). Busque cuánto debe pagar cada uno.

5. Dada una clase "Perro", darle atributos y acciones de un perro. Ejemplos: color,
raza, edad, sexo, peso; ladrar, jugar, comer, pelear, etc.

6. Dada una clase "Cuenta" represente una cuenta bancaria con:
- nombre del cliente
- número de cuenta
- saldo
- interés
- constructor (por defecto y con parámetros)
- setters/getters
- método "ingreso" que aumente el saldo
- método "giro" para disminuir el saldo
- método "transferencia" para pasar dinero de una cuenta a otra

Los últimos métodos deben devolver True para éxito o False de lo contrario, y
considerar si existe suficiente saldo.
