package me.confor.guiauno;

public class Main {
	static void print(String string) {
		System.out.println(string);
	}

	public static void main(String[] args) {
		// Ejercicio 1
		print("--- Ejercicio 1 ---");
		Tarjeta tarjeta = new Tarjeta("juanito", 1000);
		print("Saldo actual: $" + tarjeta.getSaldo());
		tarjeta.comprar(1230);
		print("Luego de comprar, el saldo es $" + tarjeta.getSaldo());

		// Ejercicio 2
		print("--- Ejercicio 2 ---");
		Libro libro = new Libro("The Call of Cthulhu", "H. P. Lovecraft", 10, 0);
		print("Hay " + libro.getEjemplares() + " copias de " + libro.getTitulo());
		libro.prestar();
		libro.prestar();
		print("Hemos prestado " + libro.getEjemplaresPrestados() + " copias.");

		// Ejercicio 3
		print("--- Ejercicio 3 ---");
		int num = 1234;
		int encriptado = Ejercicio3.encriptar(num);
		print("Número original: " + num);
		print("Número \"encriptado\": " + encriptado);

		// Ejercicio 4
		// TODO traer todos los print a esta clase
		print("--- Ejercicio 4 ---");
		Ejercicio4.pagar();

		// Ejercicio 5
		print("--- Ejercicio 5 ---");
		Perro perro = new Perro();
		perro.setColor("café");
		perro.setEdad(4);
		perro.setMacho(false);
		perro.setPeso(4.43);

		perro.actividad("ladra");
		perro.actividad("juega");
		perro.actividad("come");
		perro.actividad("pelea por terreno");

		// Ejercicio 6
		print("--- Ejercicio 6 ---");
		Cuenta cuentaUno = new Cuenta("asdasd", 1, 1000, 0);
		Cuenta cuentaDos = new Cuenta("jlkjlkj", 2, 1000, 0);

		cuentaUno.mostrarInfo();
		print("Se transfieren $500 desde " + cuentaUno.getNombre() + " a " + cuentaDos.getNombre());
		cuentaUno.transferencia(cuentaDos, 500);
		cuentaDos.mostrarInfo();


		return;
	}
}
