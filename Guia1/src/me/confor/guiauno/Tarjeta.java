package me.confor.guiauno;

public class Tarjeta {
	private String titular;
	private float saldo;

	// constructor explícito
	public Tarjeta(String titular, float saldo) {
		this.setTitular(titular);
		this.setSaldo(saldo);
	}

	public void comprar(float costo) {
		float nuevoSaldo = this.getSaldo() - costo;
		this.setSaldo(nuevoSaldo);
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getTitular() {
		return this.titular;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public float getSaldo() {
		return this.saldo;
	}
}
