package me.confor.guiauno;

public class Ejercicio4 {
	static void pagar() {
		float cuenta = 10000;
		int amigos = 4;

		// cuenta + 19% iva + 10% propina
		float iva = cuenta / 100 * 19;
		float propina = cuenta / 100 * 10;
		float total = cuenta + iva + propina;

		// cuenta dividida en pepe + amigos
		float pago = total / ( 1 + amigos );

		System.out.println("Cuenta:  $" + cuenta);
		System.out.println("IVA:     $" + iva);
		System.out.println("Propina: $" + propina);
		System.out.println("Total:   $" + cuenta);
		System.out.println("Cada uno debe pagar $" + pago);

		return;
	}
}
