package me.confor.guiauno;

public class Libro {
	private String titulo;
	private String autor;
	private int ejemplares;
	private int ejemplaresPrestados;

	// constructor por defecto
	public Libro() {}

	// constructor con parámetros
	public Libro(String titulo, String autor, int ejemplares, int ejemplaresPrestados) {
		this.setTitulo(titulo);
		this.setAutor(autor);
		this.setEjemplares(ejemplares);
		this.setEjemplaresPrestados(ejemplaresPrestados);
		return;
	}

	public void prestar() {
		this.setEjemplaresPrestados(this.getEjemplaresPrestados() + 1);
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getEjemplares() {
		return ejemplares;
	}

	public void setEjemplares(int ejemplares) {
		this.ejemplares = ejemplares;
	}

	public int getEjemplaresPrestados() {
		return ejemplaresPrestados;
	}

	public void setEjemplaresPrestados(int ejemplaresPrestados) {
		this.ejemplaresPrestados = ejemplaresPrestados;
	}
}
