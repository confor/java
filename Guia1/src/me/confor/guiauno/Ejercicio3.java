package me.confor.guiauno;

import java.util.ArrayList;
import java.util.Collections;

public class Ejercicio3 {
	static int encriptar(int num) {
		// revisar si largo == 4
		if (String.valueOf(num).length() != 4) {
			System.err.println("número inválido, tiene que tener 4 digitos");
			// throw new Exception?
			return 0;
		}

		// separar el número en un arreglo con sus digitos
		ArrayList<Integer> digits = new ArrayList<Integer>();
		ArrayList<Integer> sums = new ArrayList<Integer>();

		do {
			digits.add(num % 10);
			sums.add(0);
			num /= 10;
		} while (num > 0);

		Collections.reverse(digits);

		for (int j : digits) {
			System.out.print(j);
		}

		System.out.println("");

		for (int i = 0; i < digits.size(); i++) {
			int temp = sums.get(i);

			for (int j = 0; j <= i; j++) {
				temp += digits.get(j);
			}

			temp = temp + 7;
			temp %= 10;
			sums.set(i, temp);
		}

		Collections.swap(sums, 0, 2);
		Collections.swap(sums, 1, 3);

		int encrypted = 0;

		for (int i = 0; i < sums.size(); i++) {
			encrypted += sums.get(i) * ((int) Math.pow(10, sums.size() - i - 1));
		}

		return encrypted;
	}
}
