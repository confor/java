package me.confor.guiauno;

public class Cuenta {
	private String nombre;
	private int numero;
	private int saldo;
	private double interes;

	public Cuenta() {
		this.setNombre("");
		this.setNumero(0);
		this.setSaldo(0);
		this.setInteres(0.0);
	}

	public Cuenta(String nombre, int numero, int saldo, double interes) {
		this.setNombre(nombre);
		this.setNumero(numero);
		this.setSaldo(saldo);
		this.setInteres(interes);
	}

	public void mostrarInfo() {
		System.out.println("La cuenta de " + this.getNombre() + " tiene $" + this.getSaldo() + ".");
	}

	public boolean ingreso(int monto) {
		if (monto < 0) {
			return false;
		}

		this.setSaldo(this.getSaldo() + monto);
		return true;
	}

	public boolean giro(int monto) {
		if (monto < 0) {
			return false;
		}

		if (this.getSaldo() < monto) {
			return false;
		}

		this.setSaldo(this.getSaldo() - monto);
		return true;
	}

	public boolean transferencia(Cuenta destino, int monto) {
		if (this.giro(monto) != true) {
			return false;
		}

		return destino.ingreso(monto);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public double getInteres() {
		return interes;
	}

	public void setInteres(double interes) {
		this.interes = interes;
	}
}
