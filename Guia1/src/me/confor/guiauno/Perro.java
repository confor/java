package me.confor.guiauno;

public class Perro {
	private String color;
	private String raza;
	private float edad;
	private boolean macho;
	private double peso;

	public void actividad(String actividad) {
		System.out.println("El perro " + actividad + ".");
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public float getEdad() {
		return edad;
	}

	public void setEdad(float edad) {
		this.edad = edad;
	}

	public boolean isMacho() {
		return macho;
	}

	public void setMacho(boolean macho) {
		this.macho = macho;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double d) {
		this.peso = d;
	}
}
